import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'
import { createStore } from 'redux';
let initState = {
    users: [
        'user'
    ]
}
let reducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE': {
            return {
                users: [
                    ...state.users,
                    action.payload
                ]
            }
        }
        default: state
    }
}
let store = createStore(reducer)
// store.dispatch(
//     {
//         type: 'CREATE',
//         payload: 'user 1'
//     }
// )
// console.log(store.getState())



// console.log("store",store)
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
