import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Axios from 'axios';
// import { createUser } from './Actions'
// import { connect } from 'react-redux'
class App extends Component {
  // handleOneCreate = () => {
  //   this.props.createUser('user 2')
  // }
  state = {
    fruits: []
  }

  componentDidMount(){
    this.handleOnGetData()
  }
  handleOnGetData = () => {

    // let get = () => {
    //   return new Promise(
    //     (resolve, reject) => {
    //       setTimeout(() => {
    //         resolve("HELLO")
    //       },5000)
    //     }
    //   )
    // }

    // get().then( data => { console.log(data )}) // HELLO
    // .catch( err => {})




    Axios.get('http://localhost:3000/fruits').then(response => {
      this.setState({
        fruits:response.data
      })
    })
  }

  handleOnCreateData = () => {
    let data = {
      name: "mango",
      weight: "1 kg",
      color: "yellow",
      vitamin: "A"
    }
    Axios
      .post('http://localhost:3000/fruits',data )
      .then(response => {
        this.handleOnGetData()
      })
  }
  handleOnUpdataData = (id) => {
    let data = {
      name: "banana",
      weight: "1 kg",
      color: "yellow",
      vitamin: "A"
    }
    Axios
      .put(`http://localhost:3000/fruits/${id}`,data )
      // .then(response => {
      //   // this.setState({
      //   //   fruits: [
      //   //     ...this.state.fruits,
      //   //     response.data
      //   //   ]
      //   // })
      // })
      this.handleOnGetData()
  }
  render() {
    let { fruits } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        {/* <button onClick={this.handleOneCreate}> CREATE </button> */}
        <button onClick={this.handleOnGetData}> GET DATA </button><br />
        <button onClick={this.handleOnCreateData}> CREATE DATA </button><br />
        <button onClick={() => this.handleOnUpdataData(1)}> UPDATE DATA </button><br />
        <br />
        {
          fruits.map((value, index) => {
            //  console.log(fruits)
            let {id, name, color, vitamin } = value
            return <li key={index}>{id+" "+name + " " + color + " " + vitamin}</li>
          })
        }
      </div>
    );
  }
}

// let mapStateToProps = (store, props) => {
//   return {
//     users: store.users
//   }
// }

// let mapDispatchToProps = dispatch => ({
//   createUser: (name) => dispatch(createUser(name)),
// })



// export default connect(
// mapStateToProps,
// mapDispatchToProps
// )(App);

export default App


